#ifndef CxAODTools_WZ_TriggerTool_WWW_H
#define CxAODTools_WZ_TriggerTool_WWW_H

#include "CxAODTools/TriggerTool.h"

class TriggerTool_WWW : public TriggerTool {
 public:
  TriggerTool_WWW(ConfigStore& config);
  virtual ~TriggerTool_WWW() = default;

  virtual EL::StatusCode initTools() override;
  virtual EL::StatusCode initProperties() override;
  virtual EL::StatusCode initTriggers() override;
};

#endif  // ifndef CxAODTools_TriggerTool_WWW_H
