// Dear emacs, this is -*-c++-*-
#ifndef CxAODTools_WZ_CommonProperties_H
#define CxAODTools_WZ_CommonProperties_H

#ifndef __MAKECINT__

#include "xAODJet/Jet.h"

#include "CxAODTools/CommonProperties.h"
#include "CxAODTools/ObjectDecorator.h"

// Preprocessor macro to define property classes
//
// Usage example:
// (in ~ CxAODTools_MyAnalysis/CxAODTools_MyAnalysis/Properties_MyAnalysis.h)
//
// #include "CxAODTools/CommonProperties.h"
// PROPERTY( Props , int   , myInt   );
// PROPERTY( Props , float , myFloat );
// PROPERTY( MyProps , int   , myInt   );
// ...

// For faster compile times the declaration and instantiation can be split.
// E.g. in Properties_MyAnalysis.h:
// PROPERTY_DECL( Props , int   , myInt   );
// And in Properties_MyAnalysis.cxx:
// PROPERTY_INST( Props , int   , myInt   );

// Note, it is _never_ needed to specify your own namespace "MyProps" if the
// property already exists in another namespace, e.g. in "Props". In general,
// it's better always use namespace "Props" in your own analysis, since you
// then get an extra compile time check on whether you declared a property
// with the same name as another previously declared property but with a
// different type (if using a different namespace, this would compile, but
// fail at runtime).

// IMPORTANT :
// Don't attempt use bool properties !!!
// A bool properties gets saved as char when written to disk.
// Consequently, you cannot use the same code to access the property, when
// reading the file back in. For this reason, the bool template is disabled.

// For TrackPartHandler_WWW
// PROPERTY_DECL( Props, unsigned char , numberOfPixelHits)
// PROPERTY_DECL( Props, unsigned char , numberOfSCTHits)
// PROPERTY_DECL( Props, unsigned char , numberOfPixelSharedHits)
// PROPERTY_DECL( Props, unsigned char , numberOfSCTSharedHits)
// PROPERTY_DECL( Props, unsigned char , numberOfSCTHoles)
// PROPERTY_DECL( Props, unsigned char , numberOfPixelHoles)

// For ElectronHandler_WWW
PROPERTY_DECL(Props, int, isVetoElectron)
PROPERTY_DECL(Props, int, isNoBLElectron)
PROPERTY_DECL(Props, int, isLooseElectron)
PROPERTY_DECL(Props, int, isSignalElectron)
PROPERTY_DECL(Props, int, isTight_NoBLayerLH)
PROPERTY_DECL(Props, int, isMedium_NoBLayerLH)
PROPERTY_DECL(Props, float, trigEFFmediumLHIsoFixedCutLoose)
PROPERTY_DECL(Props, float, trigSFmediumLHIsoFixedCutLoose)
PROPERTY_DECL(Props, float, effSFIsoFixedCutLooseTightLH_PLV)
// PROPERTY_DECL(Props, int, author) author has been taken probably by jet? Use Author instead. Defined in base class
PROPERTY_DECL(Props, int, ECIDS)
PROPERTY_DECL(Props, float, ECIDSResult)
PROPERTY_DECL(Props, int, addAmbiguity)
PROPERTY_DECL(Props, int, firstEgMotherPdgId)
PROPERTY_DECL(Props, float, ptvarcone30_TightTTVALooseCone_pt1000)
// For MuonHandler_WWW
PROPERTY_DECL(Props, int, isVetoMuon)
PROPERTY_DECL(Props, int, isLooseMuon)
PROPERTY_DECL(Props, int, isSignalMuon)
PROPERTY_DECL(Props, float, fCLoose_FixedRadIso_PLVSF)

// for electrons and muons
// added this to see effect of trigger matching
//PROPERTY_DECL(Props, int, isTrigMatch)  // true if one lepton is trigger matched for triggers speccified in code

// These will hold IFFTruthClassifier information for leptons
PROPERTY_DECL(Props, int, isPrompt)     // true if lepton is prmopt (based on IFFTruthClassifier tool output)
PROPERTY_DECL(Props, int, isFake)       // true if lepton is fake (based on IFFTruthClassifier tool output)
PROPERTY_DECL(Props, int, truthStatus)  // integer holding the result of IFFTruthClassifier tool

#endif  // __MAKECINT__
#endif  //CxAODMaker_CommonProperties_WWW_H
