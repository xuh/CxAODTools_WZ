//dear emacs, this is -*- C++ -*-
#include "xAODEgamma/Electron.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/Jet.h"
#include "xAODMuon/Muon.h"
#include "xAODTau/TauJet.h"
//
#include <utility>
#include "CxAODTools_WZ/CommonProperties_WWW.h"

template <typename T>
WWWEvtSelection<T>::WWWEvtSelection() noexcept : EventSelection(), m_result() {}

// To-Do: Selection versions
template <typename T>
bool WWWEvtSelection<T>::passJetSelection(const xAOD::JetContainer* jets) {
  // look at signal jets, veto jets
  // if pass VH jets selection fill T.jets and return true
  // fill T.jets in decreasing pT order
  // else return false

  // Have to be sure to have tighter selection than pre-selection.
  // This is the same as the jet pre-selection for the time-being
  return passJetPreSelection(jets);
}

// to be implemented here
template <typename T>
int WWWEvtSelection<T>::doLeptonSelection(const xAOD::ElectronContainer* electrons, const xAOD::MuonContainer* muons,
                                          const xAOD::Electron*& el1, const xAOD::Electron*& el2, const xAOD::Electron*& el3,
                                          const xAOD::Muon*& mu1, const xAOD::Muon*& mu2, const xAOD::Muon*& mu3) {
  // one function for lepton selection to ensure consistency between 0/1/2 lepton channels
  // try to write it so that if one changes some cut for some channel, the other channels cannot become
  // overlapping/inconsistent

  // if no loose lepton, set el1, el2, mu1, mu2 to nullptr and return 0
  // if 1 tight 0 loose, set el1 or mu1 to the tight lepton, el2 and mu2 to nullptr
  // and return 1
  // if 1 medium + 1 loose, set el1 and el2 or mu1 and mu2 to the leptons, set the others to
  // nullptr, and return 2

  //  This is the same as the Lepton pre-selection for the time-being
  return doLeptonPreSelection(electrons, muons, el1, el2, el3, mu1, mu2, mu3);
}

template <typename T>
bool WWWEvtSelection<T>::passTauSelection(const xAOD::TauJetContainer* taus) {
  // look at taus
  // if pass  tau selection fill T.jets and return true
  // fill T.jets in decreasing pT order
  // else return false

  // Have to be sure to have tighter selection than pre-selection.
  // This is the same as the tau pre-selection for the time-being
  return passTauPreSelection(taus);
}

template <typename T>
bool WWWEvtSelection<T>::passSelection(SelectionContainers& containers, bool /*isKinVar*/) {
  const xAOD::EventInfo* evtinfo = containers.evtinfo;
  const xAOD::MissingET* met = containers.met;
  const xAOD::ElectronContainer* electrons = containers.electrons;
  const xAOD::MuonContainer* muons = containers.muons;
  const xAOD::TauJetContainer* taus = containers.taus;
  const xAOD::JetContainer* jets = containers.jets;
  // const xAOD::JetContainer      * fatjets   = containers.fatjets;
  // const xAOD::JetContainer      * trackjets = containers.trackjets;

  // assume that T has a field bool pass
  clearResult();

  // bad jet cleaning: recommendation is to kill the event
  if (!passJetCleaning(jets, evtinfo)) {
    m_result.pass = false;
    return false;
  }

  if (electrons && muons && met) {
    if (!passLeptonSelection(electrons, muons, met)) {
      m_result.pass = false;
      return false;
    }
  }

  //taus
  if (taus) {
    if (!passTauSelection(taus)) {
      m_result.pass = false;
      return false;
    }
  }

  // jets
  if (jets) {
    if (!passJetSelection(jets)) {
      m_result.pass = false;
      return false;
    }
  }

  //kinematics
  if (!passKinematics()) {
    m_result.pass = false;
    return false;
  }

  m_result.pass = true;
  return true;
}

// Pre-selection versions
template <typename T>
bool WWWEvtSelection<T>::passJetPreSelection(const xAOD::JetContainer* jets) {
  // assume that T has fields vector<Jet*> signalJets, forwardJets
  m_result.signalJets.clear();
  // look at signal jets, veto jets
  // if pass  jets selection fill T.jets and return true
  // fill T.jets in decreasing pT order
  // else return false
  for (unsigned int iJet(0); iJet < jets->size(); ++iJet) {
    const xAOD::Jet* jet = jets->at(iJet);
    //TODO Check correctness of this selection
    if (!Props::goodJet.get(jet)) continue;  //Loop only on good jets
    if (!Props::passOR.get(jet)) continue;
    if (Props::isSignalJet.get(jet)) {  // applied to keep CxAOD jets already
      m_result.signalJets.push_back(jet);
    }
  }
  // cannot rely on jets being pt sorted since all variations have same order in CxAOD
  std::sort(m_result.signalJets.begin(), m_result.signalJets.end(), sort_pt);
  // always true no matter what
  return true;
}

template <typename T>
int WWWEvtSelection<T>::doLeptonPreSelection(const xAOD::ElectronContainer* electrons, const xAOD::MuonContainer* muons,
                                             const xAOD::Electron*& el1, const xAOD::Electron*& el2, const xAOD::Electron*& el3,
                                             const xAOD::Muon*& mu1, const xAOD::Muon*& mu2, const xAOD::Muon*& mu3) {
  // one function for lepton selection to ensure consistency between 0/1/2 lepton channels
  // try to write it so that if one changes some cut for some channel, the other channels cannot become
  // overlapping/inconsistent

  // if no loose lepton, set el1, el2, mu1, mu2 to nullptr and return 0
  // if 1 tight 0 loose, set el1 or mu1 to the tight lepton, el2 and mu2 to nullptr
  // and return 1
  // if 1 medium + 1 loose, set el1 and el2 or mu1 and mu2 to the leptons, set the others to
  // nullptr, and return 2
  // else, return -1

  //
  el1 = nullptr;
  el2 = nullptr;
  el3 = nullptr;
  mu1 = nullptr;
  mu2 = nullptr;
  mu3 = nullptr;
  int nelecs = 0;
  int nmuons = 0;
  int nelecsSig = 0;
  int nmuonsSig = 0;

  for (unsigned int iElec = 0; iElec < electrons->size(); ++iElec) {
    const xAOD::Electron* elec = electrons->at(iElec);
    if (!Props::passOR.get(elec)) continue;
    if (Props::isLooseElectron.get(elec) || Props::isNoBLElectron.get(elec)) {
      nelecs++;
      if (el1 == 0)
        el1 = elec;
      else if (el2 == 0)
        el2 = elec;
      else if (el3 == 0)
        el3 = elec;
    }
    if (Props::isSignalElectron.get(elec)) nelecsSig++;
  }

  for (unsigned int iMuon = 0; iMuon < muons->size(); ++iMuon) {
    const xAOD::Muon* muon = muons->at(iMuon);
    if (!Props::passOR.get(muon)) continue;
    if (Props::isLooseMuon.get(muon)) {
      nmuons++;
      if (mu1 == 0)
        mu1 = muon;
      else if (mu2 == 0)
        mu2 = muon;
      else if (mu3 == 0)
        mu3 = muon;
    }
    if (Props::isSignalMuon.get(muon)) nmuonsSig++;
  }

  if (nelecs + nmuons == 0) {
    return 0;
  } else if (nelecs + nmuons == 1 && nelecsSig + nmuonsSig == 1) {
    return 1;
  } else if (nelecs + nmuons >= 2 && nelecsSig + nmuonsSig >= 1) {
    if (mu1 && mu2 && mu1->pt() < mu2->pt()) {
      std::swap(mu1, mu2);
    }
    if (mu2 && mu3 && mu2->pt() < mu3->pt()) {
      std::swap(mu2, mu3);
    }
    //Third one is needed to make sure 3>1
    if (mu1 && mu2 && mu1->pt() < mu2->pt()) {
      std::swap(mu1, mu2);
    }

    if (el1 && el2 && el1->pt() < el2->pt()) {
      std::swap(el1, el2);
    }

    if (el2 && el3 && el2->pt() < el3->pt()) {
      std::swap(el2, el3);
    }

    if (el1 && el2 && el1->pt() < el2->pt()) {
      std::swap(el1, el2);
    }
    return 2;
  }
  return -1;
}

template <typename T>
bool WWWEvtSelection<T>::passTauPreSelection(const xAOD::TauJetContainer* taus) {
  // assume that T has fields vector<TauJet*> taus
  m_result.taus.clear();
  // if pass  tau pre-selection fill T.taus and return true
  // fill T.taus in decreasing pT order
  // else return false

  for (unsigned int iTau(0); iTau < taus->size(); ++iTau) {
    const xAOD::TauJet* tau = taus->at(iTau);
    if (!Props::passTauSelector.get(tau)) continue;
    if (!Props::passOR.get(tau)) continue;
    m_result.taus.push_back(tau);
  }

  // cannot rely on taus being pt sorted since all variations have same order in CxAOD
  std::sort(m_result.taus.begin(), m_result.taus.end(), sortTaus_pt);

  // Always return true
  return true;
}

template <typename T>
bool WWWEvtSelection<T>::passPreSelection(SelectionContainers& containers, bool isKinVar) {
  const xAOD::EventInfo* evtinfo = containers.evtinfo;
  const xAOD::MissingET* met = containers.met;
  const xAOD::ElectronContainer* electrons = containers.electrons;
  const xAOD::MuonContainer* muons = containers.muons;
  const xAOD::TauJetContainer* taus = containers.taus;
  const xAOD::JetContainer* jets = containers.jets;

  // assume that T has a field bool pass
  clearResult();

  if (!isKinVar) {
    m_cutFlow.count("Preselection initial", 100);
  }

  // event cleaning
  if (evtinfo) {
    bool isMC = Props::isMC.get(evtinfo);
    // C1: Pass GRL
    if (!isMC && !Props::passGRL.get(evtinfo)) return false;
    if (!isKinVar) m_cutFlow.count("Preselection GRL", 101);

    // C2: has PV
    if (!Props::hasPV.get(evtinfo)) return false;
    if (!isKinVar) m_cutFlow.count("Preselection hasPV", 102);

    // C3: is clean event
    if (!isMC && !Props::isCleanEvent.get(evtinfo)) return false;
    if (!isKinVar) m_cutFlow.count("Preselection isCleanEvent", 103);
  }

  // C4: bad jet cleaning: recommendation is to kill the event
  if (!passJetCleaning(jets, evtinfo)) return false;

  if (!isKinVar) {
    m_cutFlow.count("Preselection jet cleaning", 104);
  }

  // leptons
  if (electrons && muons && met) {
    if (!passLeptonPreSelection(electrons, muons, met)) {
      m_result.pass = false;
      return false;
    }
  }

  //taus
  if (taus) {
    if (!passTauSelection(taus)) {
      m_result.pass = false;
      return false;
    }
  }

  if (!isKinVar) {
    m_cutFlow.count("Preselection lepton", 105);
  }

  // jets
  if (jets) {
    if (!passJetPreSelection(jets)) {
      m_result.pass = false;
      return false;
    }
  }
  if (!isKinVar) m_cutFlow.count("Preselection jet", 106);

  // Trigger to be implemented
  // if(! passTriggerPreSelection()) {
  //  m_result.pass = false;
  //  return false;
  //  }
  m_result.pass = true;
  return true;
}
