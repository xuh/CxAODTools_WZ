#!/bin/bash

echo "Try to apply Clang Format for ..."
ls */*.{icc,cxx,h} 2>/dev/null | grep -v LinkDef.h
echo "Do you want to apply the format? y/n"
read choice
if [ $choice == 'y' ] || [ $choice == 'Y' ]; then
  ls */*.{icc,cxx,h} 2>/dev/null | grep -v LinkDef.h | xargs clang-format -i -style=file
  echo "Sure. Done."
else
  echo "OK. Exit."
fi

