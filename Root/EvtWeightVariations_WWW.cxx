#include "CxAODTools_WZ/EvtWeightVariations_WWW.h"
#include "CxAODTools_WZ/CommonProperties_WWW.h"

void EvtWeightVariations_WWW::initSherpaWZ() {
  m_variations_SherpaWZ.insert(std::make_pair("MUR0.5_MUF0.5_PDF261000", 4));
  m_variations_SherpaWZ.insert(std::make_pair("MUR0.5_MUF0.5_PDF261000", 4));
  m_variations_SherpaWZ.insert(std::make_pair("MUR0.5_MUF1_PDF261000", 5));
  m_variations_SherpaWZ.insert(std::make_pair("MUR1_MUF0.5_PDF261000", 6));
  m_variations_SherpaWZ.insert(std::make_pair("MUR1_MUF1_PDF261000", 7));
  m_variations_SherpaWZ.insert(std::make_pair("MUR1_MUF2_PDF261000", 8));
  m_variations_SherpaWZ.insert(std::make_pair("MUR2_MUF1_PDF261000", 9));
  m_variations_SherpaWZ.insert(std::make_pair("MUR2_MUF2_PDF261000", 10));

  //Insert the variations into a simple unordered_set of strings
  for (auto& var : m_variations_SherpaWZ) {
    m_listOfVariations.insert(var.first);
  }
}

float EvtWeightVariations_WWW::getWeightVariation(const xAOD::EventInfo* eventInfo, const std::string& SYSNAME) {
  if (!Props::isMC.get(eventInfo)) return 1.0;  // always assign 1.0 to data
  int DSID = eventInfo->mcChannelNumber();

  if (isSherpaWZ(DSID)) {
    if (m_variations_SherpaWZ.find(SYSNAME) == m_variations_SherpaWZ.end()) return Props::MCEventWeight.get(eventInfo);
    int index = m_variations_SherpaWZ[SYSNAME];
    return Props::MCEventWeightSys.get(eventInfo).at(index);
  }

  return Props::MCEventWeight.get(eventInfo);  // return nominal value if it is not Sherpa 2.2.1 V+jets
}

bool EvtWeightVariations_WWW::isSherpaWZ(int DSID) { return (364253 == DSID); }

std::unordered_set<std::string> EvtWeightVariations_WWW::getListOfVariations() { return m_listOfVariations; }

//Check member variable m_listOfVariations for a valid systematic
bool EvtWeightVariations_WWW::hasVariation(const std::string& SYSNAME) {
  //Search m_listOfVariations for presence of said systematic
  auto result = m_listOfVariations.find(SYSNAME);
  return (result != m_listOfVariations.end());
}
