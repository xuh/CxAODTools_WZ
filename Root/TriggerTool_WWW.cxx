#include "CxAODTools_WZ/TriggerTool_WWW.h"

#include "CxAODTools_WZ/CommonProperties_WWW.h"

TriggerTool_WWW::TriggerTool_WWW(ConfigStore& config) : TriggerTool(config) {}

EL::StatusCode TriggerTool_WWW::initTools() {
  EL_CHECK("TriggerTool_WWW::initTools()", initMuonSFTool("Medium"));
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode TriggerTool_WWW::initProperties() {
  m_electronEffProp = &Props::trigEFFtightLHIsoFixedCutLoose;
  m_electronSFProp = &Props::trigSFtightLHIsoFixedCutLoose;

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode TriggerTool_WWW::initTriggers() {
  addLowestUnprescaledElectron();
  addLowestUnprescaledMuon();
  return EL::StatusCode::SUCCESS;
}
